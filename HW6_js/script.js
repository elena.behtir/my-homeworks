function createNewUser() {
    const firstName = prompt(`What is your name?`);
    const lastName = prompt(`What is your last name?`);
    const date = prompt(`Input your birthday in format dd.mm.yyyy`);
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: date,
        getAge(date) {
            const currentDate = new Date();
            const day = date.split('.')[0];
            const month = date.split('.')[1];
            const year = date.split('.')[2];
            const dateBirthday = new Date(year, month - 1, day);
            let age = Math.floor((currentDate - dateBirthday) / (365.25 * 24 * 60 * 60 * 1000));
            return age;
        },
    
        getPassword(firstName, lastName, date) {
            let firstLetterfirstName = firstName.slice(0, 1);
            const year = date.split('.')[2];
            return (`${firstLetterfirstName.toUpperCase()}${lastName.toLowerCase()}${year}`);
        }   
    }
    console.log(newUser.getAge(date));
    console.log(newUser.getPassword(firstName, lastName, date));
    return newUser;
}

console.log(createNewUser());