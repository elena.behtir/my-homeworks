const testArray = [`hello`, `world`, 23, `23`, null];
const DataType = `string`;

function filterBy(testArray, DataType) {
    return testArray.filter((item)=>{
        return (typeof item !== DataType);
    });
}

console.log(filterBy(testArray, DataType));