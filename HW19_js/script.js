// вихідні масиви даних
const ArrCommand = [2, 6, 4, 3, 5];
const ArrBacklog = [29, 135, 11, 58, 15, 66];
const DateDeadline = new Date(2022, 10, 12, 13, 45);
const CurrentDate = new Date();


// функція для розрахунку поставленої задачі

function SprintPlanning(ArrCommand, ArrBacklog, DateDeadline, CurrentDate) {

// кількість сторпоінтів, які можуть виконати розробники за 1 день
    let numStorPointCommand = ArrCommand.reduce((startValue, currValue) => {
        return startValue + currValue;
    });
    // console.log(`${numStorPointCommand} the number of stoppoints that developers can complete in 1 day`);

// кількість сторпоінтів у беклозі
    let numStorPointBacklog = ArrBacklog.reduce((startValue, currValue) => {
        return startValue + currValue;
    });
    // console.log(`${numStorPointBacklog} the number of stoppoints in backlog`);

// кількість днів та годин, необхідна для виконання завдання
    let numDays = Math.ceil(numStorPointBacklog / numStorPointCommand);
    // console.log(`${numDays} days for task performance`);

// кількість календарних днів до дедлайну
    const DaysForDeadline = Math.round((DateDeadline - CurrentDate) / (1000 * 3600 * 24));
    // console.log(CurrentDate);
    // console.log(DateDeadline);
    // console.log(`${DaysForDeadline}` + ` days`);

// кількість робочих днів до дедлайну
    let weekend = 0;
    while (CurrentDate <= DateDeadline) {
        CurrentDate.setDate(CurrentDate.getDate() + 1);
            if (CurrentDate.getDay() === 0 || CurrentDate.getDay() === 6) {
                weekend = weekend + 1;
            }
    }
    // console.log(`${weekend} weekend`);
    let WorkDaysForDeadline = DaysForDeadline - weekend;
    // console.log(`${WorkDaysForDeadline} days before deadline`);

// різниця між кількістю робочих днів до дедлайну та кількістю днів, необхідних для виконання завдання
    let diffDays = WorkDaysForDeadline - numDays;
    // console.log(diffDays);

// остаточний результат
    if (diffDays > 0) {
        console.log(`Усі завдання будуть успішно виконані за ${diffDays} днів до настання дедлайну`);
    } else {
        console.log(`Команді розробників доведеться витратити додатково ${((-diffDays) * 8)} годин після дедлайну, щоб виконати всі завдання в беклозі`);
    }
}

SprintPlanning(ArrCommand, ArrBacklog, DateDeadline, CurrentDate);