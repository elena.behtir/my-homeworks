// функція для отримання рандомних кольорів
function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
};

// кнопка "намалювати коло"
const btn = document.querySelector('.button');
btn.style.display = 'block';

// додавання input, малювання кіл, видалення кола при кліку по ньому
btn.addEventListener('click', (event) => {
    const input = document.createElement('input');
    input.type = 'text';
    input.placeholder = 'введіть число - діаметр кола';
    input.style.width = '200px';
    const button = document.createElement('button');
    button.innerText = 'Намалювати';
    document.body.append(input);
    document.body.append(button);
    input.classList.add("str-input");
    button.classList.add("button-input");      
    const btnInput = document.querySelector('.button-input');
    btnInput.style.display = 'block';
    btnInput.addEventListener('click', (e) => {
        let strInput = document.querySelector('.str-input').value;
        let container = document.createElement('div');
        container.classList.add('container');
        document.body.append(container);
        container.style.display = 'grid';
        container.style.gridTemplateColumns = `repeat(10, ${strInput}px)`;
        for (let j=0; j<100; j++) {
            let div = document.createElement('div');
            div.style.display = 'inline-block';
            div.style.width = `${strInput}px`;
            div.style.height = `${strInput}px`;
            div.style.backgroundColor = getRandomColor();
            div.style.borderRadius = '50%';
            container.append(div);
            div.addEventListener('click', (ev) => {
                div.remove();
            });
        };           
    });
});
