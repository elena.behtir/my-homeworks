let images = document.querySelectorAll('.image-to-show');
let i = 0;

const startBtn = document.querySelector('.timer-start');
const pauseBtn = document.querySelector('.timer-pause');

function slider() {
    if (i === images.length - 1) {
        images[i].style.display = 'none';
        i = 0;
        images[0].style.display = 'block';   
    } else {
        images[i].style.display = 'none';
        images[i + 1].style.display = 'block';
        i++;      
    };
};

let timer = setInterval(slider, 3000);

startBtn.addEventListener('click', evt => {
    timer = setInterval(slider, 3000);
});
pauseBtn.addEventListener('click', evt =>{
    clearInterval(timer);
});  
