let btnPass = document.querySelectorAll(`.icon-password`);

btnPass.forEach(function(btn) {
    btn.onclick = function() {
        let target = this.getAttribute('data-target');
        let inputPass = document.querySelector(target);
        if (inputPass.getAttribute(`type`) === 'password') {
            inputPass.setAttribute(`type`, 'text');
            btn.classList.add('fa-eye');
            btn.classList.remove('fa-eye-slash');
        } else {
            inputPass.setAttribute(`type`, 'password');
            btn.classList.add('fa-eye-slash');
            btn.classList.remove('fa-eye');
        }
    }
});

document.getElementById('check').onclick = function() {
    let firstPassword = document.getElementById('js-first-password').value;
    let secondPassword = document.getElementById('js-second-password').value;

    if (firstPassword === secondPassword) {
        alert('You are welcome');
    } else {
        let div = document.createElement('div');
        div.style.color = 'red';
        div.style.marginLeft = '440px';
        div.append('Потрібно ввести однакові значення');
        document.body.append(div);
    }
}

