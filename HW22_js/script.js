function createTable() {
    let table = document.createElement("table"); // створення таблиці
    table.classList.add("table");
     for (let i = 1; i <= 30; i++) { // цикл створення рядків таблиці
        let tr = document.createElement("tr"); // створення рядка
        for (let j = 1; j <= 30; j++) { // цикл створення комірок
            let td = document.createElement("td"); // створення комірки
            tr.appendChild(td); // вставка комірки в рядок
        }
        table.appendChild(tr); // вставка рядка в таблицю
    }
    document.body.appendChild(table); // вставка таблиці в body
}

createTable();

let tableTd = document.querySelector('.table');
// фарбування клітинки в чорний колір після кліку на неї та повернення їй білого кольору при повторному кліку
tableTd.addEventListener('click', (event) => { 
    event.target.classList.toggle('active');
});

// зміна кольору всіх клітинок на протилежний при кліку за межами таблиці
document.body.addEventListener('click', (evt) => {
    if (evt.target.tagName === 'BODY') {
        document.body.classList.toggle('opposite');
      }
});
