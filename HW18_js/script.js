// об'єкт, який будемо клонувати
const obj = {
  name: 'Apple MacBook Air 13"',
  price: '43000',
  properties: {
      display: '13.3" Retina (2560x1600)',
      chip: 'Apple M1',
      ram: '8GB',
      ssd: '256GB',
      os: 'MacOS Big Sur',
      color: 'gold'
  }
}
console.log(`original object`);
console.log(obj);


// функція, яка здійснює клонування об'єкта

function cloneObj() {
  let clone = {};
  for (let key in obj) {
    clone[key] = obj[key];
  }
  return clone; 
}

console.log(`cloned object`);
console.log(cloneObj(obj));




