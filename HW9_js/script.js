const testArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const testArray1 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

// Створення списку з простого масиву
function getListContent() {
  let fragment = new DocumentFragment();
  testArray.forEach((i) => {
    let numList = document.createElement('li');
    numList.append(i);
    fragment.append(numList);
  });
  return fragment;
}
ol.append(getListContent());

// Очистка сторінки від першого списку через 3 секунди
setTimeout(() => ol.remove(), 3000);

// таймер зворотного відліку
function printNumbers(from, to) {
  let current = from;
  let div = document.createElement(`div`);
  document.body.append(div);

  function go() {
    div.append(` ${current} c `);
    if (current == to) {
      clearInterval(timerId);
      div.remove();
    }
    current--;
  }
  go();
  let timerId = setInterval(go, 1000);
}
printNumbers(3, 0);

// Отримання 2-х окремих списків: із вкладеного та основного масивів
function getNestedArray(testArray1) {
  let unNumList = document.createElement(`ul`);
  testArray1.forEach((i) => {
    if (Array.isArray(i) === true) {
      unNumList.style.marginLeft = `20px`;
      getNestedArray(i); 
    } else {
      let li = document.createElement('li');
      li.textContent = i;
      unNumList.append(li); 
    }
    document.body.append(unNumList);
  });
}
getNestedArray(testArray1);