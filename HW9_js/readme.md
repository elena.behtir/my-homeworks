1. Новий HTML тег на сторінці створюється за допомогою document.createElement(tag). Наприклад, створимо новий елемент із заданим тегом <div>:
let div = document.createElement(`div`);
Для вставки елемента на сторінку використовується метод append(), наприклад:
document.body.append(div) вставить div в document.body

2. Функція insertAdjacentHTML() призначена для встави HTML з усіма тегами та іншим вмістом, прописується в коді таким чином: 
elem.insertAdjacentHTML(where, html);
Перший параметр - це спеціальне слово, що вказує куди по відношенню до elem робити вставку.
Є такі варіанти цього параметра:
`beforebegin` - вставити html безпосередньо перед `elem`;
`afterbegin` - вставити html на початок `elem`;
`beforeend` - вставити html в кінець `elem`;
`afterend` - вставити html безпосередньо після `elem`.

3. За допомогою метода remove() можна видалити елемент зі сторінки, наприклад div.remove() видалить із сторінки елемент з тегом <div>.