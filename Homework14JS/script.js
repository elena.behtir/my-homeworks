const themeBtn = document.querySelector('.change-theme');
themeBtn.onclick = () => {
    document.body.classList.toggle('dark');
    themeBtn.innerText =  document.body.classList.contains('dark') ? "Змінити тему на світлу" : "Змінити тему на темну";
    localStorage.theme = document.body.className;
}

document.body.className = localStorage.theme;