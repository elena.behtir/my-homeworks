1. Обробник подій - це функція, яка спрацьовує тоді, коли подія тільки сталася.

2. Обробник подій до елемента можна додати за допомогою метода addEventListener
elem.addEventListner('click', (e) => {дії, які потрібно виконати});
Також обробник подій може бути доданий прямо в розмітці, наприклад:
<input value = "Press" onclick = "alert ('click')" type = "button">
у цьому випадку при натисканні мишкою на кнопці виконується код, вказаний в атрибуті onclick; для вмісту атрибуту onclick використовуються одинарні лапки, а сам атрибут знаходиться у подвійних. Якщо обробник заданий через атрибут, то браузер читає HTML-розмітку, створює нову фукнцію вмісту атрибута та записує її у властивість.
Оскільки атрибут HTML-тегу не найзручніше місце для написання великої кількості коду, то краще створити окрему JavaScript функцію та викликати її там.
У елемента DOM може бути тільки одна властивість з ім'ям onclick, а метод addEventListener дозволяє додавати кілька обробників на одну подію одного елемента.
Рекомендується використовувати addEventListner, оскільки він менше навантажує браузер і швидкодія буде більшою.