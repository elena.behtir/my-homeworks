let inputPrice = document.querySelector('.input-price');

inputPrice.addEventListener('blur', (e) => {
    let val = document.querySelector('.input-price').value;
    if (val <=0) {
        inputPrice.style.borderColor = "#FF0000";
        let elem = document.createElement('div');
        elem.innerText = 'Please enter correct price';
        elem.style.textAlign = 'center';
        elem.style.color = '#FF0000';
        document.body.append(elem);
    } else {   
        inputPrice.style.color = 'rgb(19, 242, 31)';
        let div = document.createElement('div');
        div.classList.add('span-container');
        document.body.prepend(div);
        let span = document.createElement('span');
        span.innerText = 'Поточна ціна: ' + val + '$';
        span.classList.add('span');
        div.appendChild(span);
        let button = document.createElement('button');
        button.innerText = 'X';
        button.classList.add('btn');
        div.appendChild(button);
    
    // видалення span після натискання на хрестик та очищення input
        let btn = document.querySelector('.btn');
        btn.addEventListener('click', (ev) => {
            div.remove();
            document.querySelector('.input-price').value = "";
            inputPrice.style.color = '#000000';
        });
    }    
});
