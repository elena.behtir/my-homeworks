function point() {  // клас комірка
    this.is_mine=false; // чи є міна
    this.mine_around=0; // міни поруч
    this.is_open=false; // чи відкрита клітинка
};

    let game = { // об'єкт-гра
        width:8, // ширина поля
        height:8, // висота поля
        mine_count:10, // кількість мін
        open_count:0, // кількість відкритих мін
        field:[], // поле
        fill_field:function() { // метод заповнення поля
            this.field=[]; // очищення поля
            for (let i=0; i<this.width; i++) { // проходимо по колонкам
                let tmp = []; // створюємо колонку
                for (let j=0; j<this.height; j++) { // заповнюємо її комірками
                    tmp.push(new point());
                }
                this.field.push(tmp); // вставляємо колонку в поле
            }

            // цикл розстановки мін
            for (let i = 0; i<this.mine_count;) { // поки не всі міни розставлені
                let x = parseInt(Math.random()*this.width-0.0001); // генеруємо координату X
                let y = parseInt(Math.random()*this.height-0.0001); // генеруємо координату Y
                if (!(this.field[x][y].is_mine)) { // якщо тут ще нема міни
                    
                    this.field[x][y].is_mine=true; // ставимо міну
              
                    i++; // рахуємо її
                    // console.log(this.field[x][y]);

                }
            }

        },
        mine_around_counter:function(x,y) { // рахує кількість мін навколо комірки та записує його в неї
            let x_start = (x>0) ? (x-1) : x;
            let y_start = (y>0) ? (y-1) : y;
            let x_end = x < (this.width-1) ? (x+1) :x;
            let y_end = y < (this.height-1) ? (y+1) : y;
            let count = 0;
            for (let i = x_start; i <= x_end; i++) { 
                for (let j = y_start; j <= y_end; j++) {
                    if (this.field[i][j].is_mine && !(x==i && y==j)) count++;
                }
            }
            this.field[x][y].mine_around=count;
        },    

        start_mine_counter: function() { // проходить по всім коміркам та викликає розрахунок кількості мін поруч
            for (let i = 0; i < this.width; i++) {
                for (let j = 0; j < this.height; j++) {
                    this.mine_around_counter(i,j);
                }
            }
        },

        start:function() { // "нова гра" перестворює нове поле
            this.open_count = 0;
            this.fill_field();
            this.start_mine_counter();
        }
    }

let page = {
    init: function() {
        this.game_interface.init();
    },
    game_interface: {
        table: null,
        init:function() {
            game.start(); // генерація нового поля
            this.div = document.querySelector('.field'); // отримуємо блок для виводу
            this.draw_field(); // викликаємо відмальовування поля
            let self = this; // зберігаємо this 
            this.div.addEventListener('click', (e) => {
                if (e.target.matches('td') &&!(e.target.matches('.lock'))) self.open(e);
            });
            this.div.addEventListener('contextmenu', (e) => {
                if (e.target.matches('td')) self.lock(e);
            });
        },
        draw_field:function() { // відмальовування поля
            this.div.innerHTML = ""; // очищування поля
            let table = document.createElement("table"); // створення основної таблиці
            this.table = table; // збереження таблиці в поле класу
            for (let i = 0; i < game.height; i++) { // цикл створення рядків таблиці
                let tr = document.createElement("tr"); // створення рядка
                for (let j = 0; j < game.width; j++) { // цикл створення комірок
                    let td = document.createElement("td"); // створення комірки
                    if (game.field[i][j].is_mine) {
                        td.classList.add('bomb');
                    }
                    tr.appendChild(td); // вставка комірки в рядок
                }
                table.appendChild(tr); // вставка рядка в таблицю
            }
            this.div.appendChild(table); // вставка таблиці в основний блок
        },
        open: function(e) { // обробник кліка по комірці
            x = e.target.cellIndex; // отримання номера стовпчика
            y = e.target.parentNode.rowIndex; // отримання номера рядка
            this.recurse_open(x, y); // відкриваємо комірку
        },
        recurse_open: function(x, y) { // функція рекурсивного відкривання комірок
            let td = this.table.rows[y].children[x]; // отримуємо комірку по координатам
            if (game.field[x][y].is_open) return; // якщо вже відкривали, то виходимо
            if (game.field[x][y].is_mine) { // якщо натрапили на міну, то видаємо сповіщення про програш          
                alert('Ви програли');
                let bobms = document.querySelectorAll('.bomb');
                bobms.forEach((bomb) => {
                    console.log(bomb);
                    bomb.style.opacity = "1";
                });   
                game.start();
                this.draw_field();
            } else { // якщо міни нема
                td.innerHTML = game.field[x][y].mine_around; // записуємо в комірку таблиці кількість мін поруч
                game.field[x][y].is_open = true; // ставимо позначку, що комірка відкрита
                td.classList.add('open'); // додаємо клас відкритої комірки
                game.open_count++; // інкрементуємо лічильник відкритих комірок
                if ((game.width * game.height - game.mine_count) == game.open_count) { // якщо комірка остання
                    alert ("Ви виграли!!!"); // ПЕРЕМОГА!
                    game.start();
                    this.draw_field();
                }
                if (game.field[x][y].mine_around == 0) { // якщо поруч мін нема, то 
                    for (let i = (x > 0) ? (x - 1) : x; i < x + 1 && i < game.width; i++) { // проходимся по всім сусіднім коміркам
                        for (let j = (y > 0) ? (y - 1) : y; j < game.height; j++) {
                            this.recurse_open(i,j); // та відкриваємо їх
                        }
                    }
                }             
            }
        },
        lock: function(e) { // блокування
            x = e.target.cellIndex;
            y = e.target.parentNode.rowIndex;
            if (game.field[x][y].is_open) return;
            e.target.classList.toggle('lock');
            e.preventDefault();
        }
    },

};

window.onload=function() {
    page.init();
}







// startGame(8, 8, 10);

// function startGame(WIDTH, HEIGTH, BOMBS_COUNT) {
//     const field = document.querySelector('.field');
//     const cellsCount = WIDTH * HEIGTH;
//     field.innerHTML = '<button></button>'.repeat(cellsCount);
//     const cells = [...field.children];

//     let closedCount = cellsCount;

//     const bombs = [...Array(cellsCount).keys()].sort(() => Math.random() - 0.5).slice(0, BOMBS_COUNT);

//     field.addEventListener('click', (event) => {
//         if (event.target.tagName !== 'BUTTON') {
//             return;
//         };
//         const index = cells.indexOf(event.target);
//         const column = index % WIDTH;
//         const row = Math.floor(index / WIDTH);
//         open(row, column);
//     });

//     function isValid(row, column) {
//         return row >= 0 && row < HEIGTH && column >= 0 && column < WIDTH;
//     };

//     function getCount(row, column) {
//         let count = 0;
//         for (let x = -1; x <= 1; x++) {
//             for (let y = -1; y <= 1; y++) {
//                 if (isBomb(row + y, column + x)) {
//                     count++;
//                 };
//             };
//         };
//         return count;
//     };

//     function open(row, column) {
//         if (!isValid(row, column)) return;

//         const index = row * WIDTH + column;
//         const cell = cells[index];

//         if (cell.disabled === true) return;

//         cell.disabled = true;


//         if (isBomb(row, column)) {
//             cell.innerHTML = "X";
//             alert("Ви програли!");
//             return;
//         };

//         closedCount--;
//         if (closedCount <= BOMBS_COUNT) {
//             alert('Ви виграли!!!');
//             return;
//         }

//         const count = getCount(row, column);

//         if (count !== 0) {
//             cell.innerHTML = count;

//             return;
//         };

//         for (let x = -1; x <= 1; x++) {
//             for (let y = -1; y<= 1; y++) {
//                 open(row + y, column + x)
//             };
//         };   
//     };

//     function isBomb(row, column) {
//         if (!isValid(row, column)) return false;
//         const index = row * WIDTH + column;
//         return bombs.includes(index);
//     };
// };