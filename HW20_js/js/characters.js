const characters = [
    {
        "name": "Luke Skywalker",
        "films": [
            "New", "Hope",
            "Empire", "Strikes", "Back",
            "Return", "Jedi",
            "Revenge", "Sith"
        ]
    },
    {
        "name": "C-3PO",
        "films": [
            "A New Hope",
            "New", "Hope",
            "The Empire Strikes Back",
            "Return of the Jedi",
            "Return", "Jedi",
            "The Phantom Menace",
            "Attack", "Clones",
            "Revenge of the Sith",
            "Revenge", "Sith"
        ]
    },
    {
        "name": "R2-D2",
        "films": [
            "New", "Hope",
            "Empire", "Strikes", "Back",
            "Return", "Jedi",
            "Revenge", "Sith"
        ],
        },
    {
        "name": "Darth Vader",
        "films": [
            "A New Hope",
            "New", "Hope",
            "The Empire Strikes Back",
            "Return of the Jedi",
            "Return", "Jedi",
            "Revenge of the Sith",
            "Revenge", "Sith"
        ]
    },
    {
        "name": "Leia Organa",
        "films": [
            "A", "New", "Hope",
            "The Empire Strikes Back",
            "Return of the Jedi",
            "Return", "Jedi",
            "Revenge of the Sith"
        ]
    },
    {
        "name": "Owen Lars",
        "films": [
            "New", "Hope",
            "Attack of the Clones",
            "Revenge of the Sith"
        ]
    },
    {
        "name": "Beru Whitesun lars",
        "films": [
            "A New Hope",
            "New", "Hope",
            "Attack of the Clones",
            "Revenge of the Sith"
        ]        
    },
    {
        "name": "R5-D4",
        "films": [
            "A New Hope",
            "New", "Hope"
        ]
    },
    {
        "name": "Biggs Darklighter",
        "films": [
            "A New Hope",
            "New", "Hope"
        ]
    },
    {
        "name": "Obi-Wan Kenobi",
        "films": [
            "A New Hope",
            "New", "Hope",
            "The Empire Strikes Back",
            "Return of the Jedi",
            "Return", "Jedi",
            "Phantom", "Menace",
            "Attack of the Clones",
            "Revenge of the Sith"
        ]
    },
    {
        "name": "Anakin Skywalker",
        "films": [
            "The Phantom Menace",
            "Attack of the Clones",
            "Revenge of the Sith"
        ]
    },
    {
        "name": "Wilhuff Tarkin",
        "films": [
            "A New Hope",
            "New", "Hope",
            "Revenge of the Sith"
        ]
    },
    {
        "name": "Chewbacca",
        "films": [
            "A New Hope",
            "New", "Hope",
            "The Empire Strikes Back",
            "Return of the Jedi",
            "Return", "Jedi",
            "Revenge of the Sith"
        ]
    },
    {
        "name": "Han Solo",
        "films": [
            "A New Hope",
            "New", "Hope",
            "The Empire Strikes Back",
            "Return of the Jedi",
            "Return", "Jedi"
        ]
    },
    {
        "name": "Greedo",
        "films": [
            "A New Hope",
            "New", "Hope"
        ]
    },
    {
        "name": "Jabba Desilijic Tiure",
        "films": [
            "A New Hope",
            "New", "Hope",
            "Return of the Jedi",
            "Return", "Jedi",
            "The Phantom Menace"
        ]
    },
    {
        "name": "Wedge Antilles",
        "films": [
            "A New Hope",
            "New", "Hope",
            "The Empire Strikes Back",
            "Return of the Jedi",
            "Return", "Jedi"
        ]
    },
    {
        "name": "Jek Tono Porkins",
        "films": [
            "A New Hope",
            "New", "Hope"
        ]
    },
    {
        "name": "Yoda",
        "films": [
            "The Empire Strikes Back",
            "Return of the Jedi",
            "Return", "Jedi",
            "The Phantom Menace",
            "Attack of the Clones",
            "Revenge of the Sith"
        ]
    },
    {
        "name": "Palpatine",
        "films": [
            "The Empire Strikes Back",
            "Return of the Jedi",
            "Return", "Jedi",
            "The Phantom Menace",
            "Attack of the Clones",
            "Revenge of the Sith"
        ]
    },
    {
        "name": "Boba Fett",
        "films": [
            "The Empire Strikes Back",
            "Return of the Jedi",
            "Return", "Jedi",
            "Attack of the Clones"
        ]
    },
    {
        "name": "IG-88",
        "films": [
            "The Empire Strikes Back"
        ]
    },
    {
        "name": "Bossk",
        "films": [
            "The Empire Strikes Back"
        ]
    },
    {
        "name": "Lando Calrissian",
        "films": [
            "The Empire Strikes Back",
            "Return of the Jedi",
            "Return", "Jedi"
        ]
    },
    {
        "name": "Lobot",
        "films": [
            "The Empire Strikes Back"
        ]
    },
    {
        "name": "Ackbar",
        "films": [
            "Return of the Jedi",
            "Return", "Jedi"
        ]
    },
    {
        "name": "Mon Mothma",
        "films": [
            "Return of the Jedi",
            "Return", "Jedi"
        ]
    },
    {
        "name": "Arvel Crynyd",
        "films": [
            "Return of the Jedi",
            "Return", "Jedi"
        ]
    },
    {
        "name": "Wicket Systri Warrick",
        "films": [
            "Return of the Jedi",
            "Return", "Jedi"
        ]
    },
    {
        "name": "Nien Nunb",
        "films": [
            "Return of the Jedi",
            "Return", "Jedi"
        ]
    },
    {
        "name": "Qui-Gon Jinn",
        "films": [
            "The Phantom Menace"
        ]
    },
    {
        "name": "Nute Gunray",
        "films": [
            "The Phantom Menace",
            "Attack of the Clones",
            "Revenge of the Sith"
        ]
    },
    {
        "name": "Finis Valorum",
        "films": [
            "The Phantom Menace"
        ]
    }
]
