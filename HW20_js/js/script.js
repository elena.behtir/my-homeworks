// вихідні дані
const keywords = `A New Hope`;
const boolean = true;
// const boolean = false;

// функція
function filterCollection(characters, keywords, boolean) {
    let persons = [];
    if (boolean === true) {
        characters.forEach((character) => {
            if (character.films.includes(keywords)) {
                persons.push(character.name);
            }
        });
        return persons;
    } else {
        let keyword1 = keywords.split(" ")[0];
        let keyword2 = keywords.split(" ")[1];
        let keyword3 = keywords.split(" ")[2];
        characters.forEach((character) => {
            if (character.films.includes(keyword1) || character.films.includes(keyword2) || character.films.includes(keyword3)) {
                persons.push(character.name);
            }           
         });
        return persons;
    }
}

console.log(filterCollection(characters, keywords, boolean));
