// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraph = document.querySelectorAll(`p`);
paragraph.forEach((el) => {
    el.style.backgroundColor = `#ff0000`;
});


// Знайти елемент із id="optionsList". Вивести у консоль. 
console.log(document.getElementById("optionsList"));

// Знайти батьківський елемент та вивести в консоль. 
console.log(optionsList.parentElement);

//Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод
console.log(optionsList.hasChildNodes());
console.log(optionsList.childNodes);

const childOptionsList = optionsList.childNodes;
childOptionsList.forEach((el, idx) => {
    if (optionsList.childNodes[idx].nodeType === 1) {
        console.log(`${(optionsList.childNodes[idx].nodeName)} - nodeName, ` + `nodeType: ${(optionsList.childNodes[idx].nodeType)} - ELEMENT_NODE`);
        } else {
            if (optionsList.childNodes[idx].nodeType === 3) {
                console.log(`${(optionsList.childNodes[idx].nodeName)} - nodeName, ` + `nodeType: ${(optionsList.childNodes[idx].nodeType)} - TEXT_NODE`);
            } else {
            console.log(`${(optionsList.childNodes[idx].nodeName)} - nodeName, ` + `${(optionsList.childNodes[idx].nodeType)} - nodeType`);
            }    
    } 
});


// Встановити в якості контента елемента testParagraph наступний параграф - "This is a paragraph"
const testParagraph = document.getElementById(`testParagraph`);
testParagraph.firstChild.remove();
testParagraph.append(`This is a paragraph`);


// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
console.log(document.querySelector(`.main-header`).childNodes);
const list = document.querySelector(`.main-header`);
function changeClass(elem) {
    let elmnts = Array.from(elem.children);
    for (let e of elmnts) {
        e.classList.add(`nav-item`);
    }
}
changeClass(list);


// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const sectionTitle = Array.from(document.querySelectorAll(`.section-title`));
console.log(sectionTitle);
sectionTitle.forEach((el) => {
    el.classList.remove(`section-title`);
});